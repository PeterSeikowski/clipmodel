import os
import torch
from torch.utils.data import Dataset, DataLoader
from PIL import Image
from torchvision import transforms
import torch.nn as nn
from transformers import AutoTokenizer







class CLIPDataset(Dataset):

    def __init__(self,tokenizer_name, filepath, image_size, padding_size= 44, normalize= False):
        super(CLIPDataset, self).__init__()
        self.tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)          # Tokenizer used to tokenize the words
        self.filepath = filepath
        self.image_size = image_size                                            # Hight/Width the images are getting reshaped to 
        self.normalize = normalize                                              # Normalize image tensor if True
        self.padding_size = padding_size                                        # paddings size
        try: 
            self.image_files = os.listdir(f'{filepath}/Images')     # List of names of the images')
        except Exception as e:
            print('Make sure to name the Image Folder "Images"')

        # Dictionary {'image filename' : caption }
        self.captions = {}                      

        with open(f'{filepath}/captions.txt') as file:
            next(file)
            for line in file:
                image, caption = line.split(',',1)
                self.captions[image] = caption.replace('\n', '')    

        
    def get_max_sentence_length(self):
        # Returns the maximal length of any sentence in the Dataset + 2 for SOS,EOS token
        max_length = max(len(sentence.split()) for sentence in self.captions.values())
        return max_length + 2

    def __len__(self):
        return len(self.image_files)
        
        
    def __getitem__(self, idx):
        image = Image.open(self.filepath + '/Images/' + self.image_files[idx])      # Load image
        image = image.resize((self.image_size,self.image_size))                     # Resize image

        if self.normalize == True:
            # Normalize image 
            mean = [0.485, 0.456, 0.406]                            # Mean for RGB
            std = [0.229, 0.224, 0.225]                             # Std for RGB
            normalize = transforms.Normalize(mean=mean, std=std)

            image_tensor = transforms.ToTensor()(image)
            image_tensor = normalize(image_tensor)

        else:
            image_tensor = transforms.ToTensor()(image)
            
    

        caption = self.captions[self.image_files[idx]]                                                           # Get the idx'th caption                                                            
        tokenized_caption = self.tokenizer(caption, padding= 'max_length', max_length= self.padding_size)        # Tokenize the caption with padding


        return (image_tensor, tokenized_caption)       # tokenized_caption['input_ids','token_type_ids', 'attention_mask'] each key returns list of tensors
    

    



