from transformers import AutoTokenizer
from CLIPDataset import CLIPDataset
import torch.nn as nn
import torch
from torch.utils.data import DataLoader
import math


class AttentionBlock(nn.Module):

    def __init__(
            self, 
            input_size, 
            embedding_size, 
            num_heads
    ):
        super(AttentionBlock,self).__init__()
        self.input_size = input_size            # Length of the input sequence
        self.num_heads = num_heads              # Number of attention heads
        self.embedding_size = embedding_size    # Size of vector representation 
        if embedding_size % num_heads != 0:
            raise ValueError('embedding_size must be a multiple of num_heads')
        self.head_len = int(self.embedding_size/self.num_heads)
        
        self.q_layer = nn.Linear(self.embedding_size,self.embedding_size)
        self.k_layer = nn.Linear(self.embedding_size,self.embedding_size)
        self.v_layer = nn.Linear(self.embedding_size,self.embedding_size)

        self.out_layer = nn.Linear(self.embedding_size, self.embedding_size)



    def forward(self, embeddings, attention_masks= None):

        def seperate_heads(x):
            return x.view(-1,self.input_size, self.num_heads, self.head_len).transpose(1,2)
        
        def group_heads(x):
            return x.transpose(1,2).contiguous().view(-1,self.input_size, self.embedding_size)

        q = self.q_layer(embeddings)    # batch_size x input_size x embedding_size
        k = self.k_layer(embeddings)    # batch_size x input_size x embedding_size
        v = self.v_layer(embeddings)    # batch_size x input_size x embedding_size


        # Seperating the heads
        q = seperate_heads(q)   # batch_size x num_heads x input_size x head_len
        k = seperate_heads(k)   # batch_size x num_heads x input_size x head_len
        v = seperate_heads(v)   # batch_size x num_heads x input_size x head_len

        ## SelfAttention
        # Scaling q 
        q = q/math.sqrt(self.head_len)

        attn_weights = torch.softmax(torch.matmul(q,k.transpose(2,-1)), dim=-1)     # batch_size x num_heads x input_size x input_size

        if attention_masks != None:
            attention_masks_unsqueezed = attention_masks.unsqueeze(1).expand(-1,self.num_heads,-1).unsqueeze(-2)       # Unsqueeze and expand into dimension of num_heads, then unsqueeze second last dimension, such that the mask is applied row-wise
            attn_weights = attn_weights * attention_masks_unsqueezed                                                   # Apply attention mask

        attn_vector =  torch.matmul(attn_weights,v)                                 # batch_size x num_heads x input_size x head_len

        attn_vector = group_heads(attn_vector)
        output = self.out_layer(attn_vector)

        return output
    
    


class TransformerEncoder(nn.Module):

    def __init__(
            self,
            input_size,         # Sequence length
            embedding_size,     # size of vector representation
            num_heads,          # Number of Self Attention Heads
            num_layers,         # Number of Attention Layers
            lin_hidden_size,    # hidden size of the FFN in the attention layers

    ):
        super(TransformerEncoder, self).__init__()
        self.input_size = input_size
        self.embedding_size = embedding_size
        self.num_heads = num_heads
        self.num_layers = num_layers
        self.lin_hidden_size = lin_hidden_size

        self.MLP_layer = nn.Sequential(                                                 # MLP after the Attentionblock
            nn.Linear(self.embedding_size, self.lin_hidden_size),
            nn.ReLU(),
            nn.Linear(self.lin_hidden_size,self.embedding_size)
        )            

        self.attention_block = AttentionBlock(self.input_size, self.embedding_size, self.num_heads)      # AttentionBlock
        self.layer_norm = nn.LayerNorm(self.embedding_size)                                              # LayerNormalization 



    def forward(self, inputs, attention_masks = None):
        # inputs.shape batch_size x input_size x embedding_size

        representation_temp = inputs


        for i in range(self.num_layers):

            if attention_masks != None:
                attention_vector = self.attention_block(representation_temp,attention_masks)    # Attention with mask
            else:
                attention_vector = self.attention_block(representation_temp)                    # Attention without mask

            representation_temp = representation_temp + attention_vector                # Residual Connection
            representation_temp = self.layer_norm(representation_temp)                  # Layer Normalization

            representation_forwarded = self.MLP_layer(representation_temp)              # Feed Forward NN
            representation_forwarded = representation_forwarded + representation_temp   # Residual Connection
            representation_forwarded = self.layer_norm(representation_forwarded)        # Layer Normalization

            representation_temp = representation_forwarded

        output = representation_temp
               
        return output               # Shape: batch_size x input_size/sequence_length x embeddings_size





class TextEmbedding(nn.Module):

    def __init__(self, sequence_length, embed_dim, vocab_size):
        super(TextEmbedding, self).__init__()
        self.sequence_length = sequence_length                                  # Sequence length of Dataset
        self.embed_dim = embed_dim                                              # Dimension of word embeddings
        self.vocab_size = vocab_size                                            # Size of vocabulary

        self.embedding_layer = nn.Embedding(self.vocab_size, self.embed_dim, padding_idx= 0)       # Layer for word embeddings


    def create_position_encoding(self):
        d_model = self.embed_dim            # Embeddings size
        seq_len = self.sequence_length      # Sequence length        

        denominator = 1000**((torch.arange(0,int(d_model/2)))/d_model)                                      # Creates a denominator vector of size d_model/2
        positions = torch.arange(1,seq_len+1).repeat_interleave(int(d_model/2)).view(-1,int(d_model/2))     # Creates a position tensor of shape seq_len x d_model/2
        embedding_sin = torch.sin(positions/denominator)        
        embedding_cos = torch.cos(positions/denominator)
        pos_embedding = torch.zeros(seq_len,d_model)
        pos_embedding[:,0::2] = embedding_sin                       # At every second position starting from 0, put the sin values
        pos_embedding[:,1::2] = embedding_cos                       # At every second position starting from 1, put the cos values

        return pos_embedding        # pos_embeddings.shape = seq_len x embedding_dimension
    


    def forward(self, inputs):
        # inputs.size = batch_size x sequence_length

        embeddings = self.embedding_layer(inputs)               # shape: batch_size x sequence_length x embedding_dimension
        position_embedding = self.create_position_encoding()    # shape:              sequence_length x embedding_dimension

        embeddings = embeddings + position_embedding

        return embeddings           # shape: batch_size x sequence_length x embedding_dimension



class TextTransformerEncoder(nn.Module):

    def __init__(
            self,
            sequence_length,              # Length of input sequences
            embedding_dim,                # Size of word embeddings
            vocab_size,                   # Size of vocabulary
            num_attention_heads,          # Number of Self Attention Heads
            num_attention_layers,         # Number of Attention Layers
            lin_hidden_size,              # Hidden size of the FFN in the attention layers
    ):
        super(TextTransformerEncoder,self).__init__()
        self.sequence_length     = sequence_length
        self.embedding_dim       = embedding_dim
        self.vocab_size          = vocab_size
        self.num_attention_heads = num_attention_heads
        self.num_attention_layers = num_attention_layers
        self.lin_hidden_size     = lin_hidden_size

        self.text_embedding = TextEmbedding(
            sequence_length,
            embedding_dim,
            vocab_size
        )

        self.transformer_encoder = TransformerEncoder(
            input_size      = sequence_length,
            embedding_size  = embedding_dim,
            num_heads       = num_attention_heads,
            num_layers      = num_attention_layers,
            lin_hidden_size = lin_hidden_size

        )

    def forward(self,text_batch, attention_masks= None):

        embedded_text_batch = self.text_embedding(text_batch)                                 # Embed the given texts
        encoded_text_batch = self.transformer_encoder(embedded_text_batch,attention_masks)    # Feed the embedded texts and attentionmasks to the encoder
        representation_tokens = encoded_text_batch[:,0,:]                                     # Take the embeddings of the first token (assume that the overall meaning of the sentence is stored there)

        return representation_tokens        # Shape: batch_size x embeddings_size

        