import torch
import torch.nn as nn
import math

class AttentionBlock(nn.Module):

    def __init__(
            self, 
            input_size, 
            embedding_size, 
            num_heads
    ):
        super(AttentionBlock,self).__init__()
        self.input_size = input_size            # Length of the input sequence
        self.num_heads = num_heads              # Number of attention heads
        self.embedding_size = embedding_size    # Size of vector representation 
        if embedding_size % num_heads != 0:
            raise ValueError('embedding_size must be a multiple of num_heads')
        self.head_len = int(self.embedding_size/self.num_heads)
        
        self.q_layer = nn.Linear(self.embedding_size,self.embedding_size)
        self.k_layer = nn.Linear(self.embedding_size,self.embedding_size)
        self.v_layer = nn.Linear(self.embedding_size,self.embedding_size)

        self.out_layer = nn.Linear(self.embedding_size, self.embedding_size)



    def forward(self, embeddings):

        def seperate_heads(x):
            return x.view(-1,self.input_size, self.num_heads, self.head_len).transpose(1,2)
        
        def group_heads(x):
            return x.transpose(1,2).contiguous().view(-1,self.input_size, self.embedding_size)

        q = self.q_layer(embeddings)    # batch_size x input_size x embedding_size
        k = self.k_layer(embeddings)    # batch_size x input_size x embedding_size
        v = self.v_layer(embeddings)    # batch_size x input_size x embedding_size


        # Seperating the heads
        q = seperate_heads(q)   # batch_size x num_heads x input_size x head_len
        k = seperate_heads(k)   # batch_size x num_heads x input_size x head_len
        v = seperate_heads(v)   # batch_size x num_heads x input_size x head_len

        ## SelfAttention
        # Scaling q 
        q = q/math.sqrt(self.head_len)

        attn_weights = torch.softmax(torch.matmul(q,k.transpose(2,-1)), dim=-1)     # batch_size x num_heads x input_size x input_size
        attn_vector =  torch.matmul(attn_weights,v)                                 # batch_size x num_heads x input_size x head_len

        attn_vector = group_heads(attn_vector)
        output = self.out_layer(attn_vector)

        return output
    
    


class TransformerEncoder(nn.Module):

    def __init__(
            self,
            input_size,         # Sequence length
            embedding_size,     # size of vector representation
            num_heads,          # Number of Self Attention Heads
            num_layers,         # Number of Attention Layers
            lin_hidden_size,    # hidden size of the FFN in the attention layers

    ):
        super(TransformerEncoder, self).__init__()
        self.input_size = input_size
        self.embedding_size = embedding_size
        self.num_heads = num_heads
        self.num_layers = num_layers
        self.lin_hidden_size = lin_hidden_size

        self.MLP_layer = nn.Sequential(                                                 # MLP after the Attentionblock
            nn.Linear(self.embedding_size, self.lin_hidden_size),
            nn.ReLU(),
            nn.Linear(self.lin_hidden_size,self.embedding_size)
        )            

        self.attention_block =       AttentionBlock(self.input_size, self.embedding_size, self.num_heads)           # AttentionBlock
        self.layer_norm = nn.LayerNorm(self.embedding_size)                                                         # LayerNormalization 




    def forward(self, inputs):
        # inputs.shape batch_size x input_size x embedding_size

        representation_temp = inputs
        residuals = inputs

        for i in range(self.num_layers):
            attention_vector = self.attention_block(representation_temp)                # Attention
            representation_temp = representation_temp + attention_vector                # Residual Connection
            representation_temp = self.layer_norm(representation_temp)                  # Layer Normalization

            representation_forwarded = self.MLP_layer(representation_temp)              # Feed Forward NN
            representation_forwarded = representation_forwarded + representation_temp   # Residual Connection
            representation_forwarded = self.layer_norm(representation_forwarded)        # Layer Normalization

            representation_temp = representation_forwarded

        output = representation_temp
               
        return output





class ImageEmbedding(nn.Module):

    def __init__(
            self,
            dimensions,              # Tuple (Channel x Hight x Width)
            num_patches,             # Number of patches per dimension
            proj_embedding_size,     # Length of patch embeddings after linear projection
    ):
        super(ImageEmbedding,self).__init__()
        self.num_patches = num_patches                              # Number of patches per dimension
        self.num_total_patches = num_patches**2                     # Number of total patches
        self.dimensions = dimensions                                # (Channel x Hight x Width)
        self.patch_size = int(self.dimensions[1]/self.num_patches)  # length of a single patch
        self.embedding_size = self.patch_size**2                    # Length of patch embeddings before linear projection
        self.proj_embedding_size = proj_embedding_size              # Length of patch embeddings after linear projection


        self.linear_projection = nn.Linear(self.embedding_size,self.proj_embedding_size )       # Linear transformation of patches
        self.class_token = nn.Parameter(torch.randn(1,self.proj_embedding_size))                # Class token to be learned by the model


        
    def patch_images(self,images):
        # Images = batch_size x Channel x Hight x Width

        if len(images.shape) != 4:                                                      # Check if a batch is given
            raise ValueError('input a full batch or use unsqueeze() for single image')  
        
        if images.shape[2] != images.shape[3]:                                          # Check if images are square
            raise ValueError('Image is not square')
        
        if images.shape[2] % self.patch_size != 0:                                      # Check if image dimension and patch size fit
            raise ValueError('Image dimension and patch size does not match')
        
        num_channels = images.shape[1]
        patched_image = images.unfold(2, self.patch_size, self.patch_size)                                                          # Unfold the third dimension (Hight)
        patched_image = patched_image.unfold(3, self.patch_size, self.patch_size)                                                   # Unfold the fourth dimension (Width)
        patched_image = patched_image.contiguous().view(-1, num_channels, self.num_patches, self.num_patches, self.embedding_size)  # Flatten the patches
        patched_image = patched_image.view(-1,num_channels, self.num_total_patches, self.embedding_size)                            # Stack patch embeddings in one dimension
        patched_image = patched_image.view(-1,num_channels * self.num_total_patches, self.embedding_size)                           # Stack Channels in one dimension
        
        return patched_image            # patched_image.shape = batch_size x (Channels * total number of patches) x pixel per batch(embedding_size)
    

    def create_position_encoding(self):
        d_model = self.proj_embedding_size
        seq_len = (self.num_total_patches * self.dimensions[0]) +1    # Sequence_length.shape  = (number of total patches * number of channels) + 1 (class token)

        denominator = 1000**((torch.arange(0,int(d_model/2)))/d_model)                                      # Creates a denominator vector of size d_model/2
        positions = torch.arange(1,seq_len+1).repeat_interleave(int(d_model/2)).view(-1,int(d_model/2))     # Creates a position tensor of shape seq_len x d_model/2
        embedding_sin = torch.sin(positions/denominator)        
        embedding_cos = torch.cos(positions/denominator)
        pos_embedding = torch.zeros(seq_len,d_model)
        pos_embedding[:,0::2] = embedding_sin                       # At every second position starting from 0, put the sin values
        pos_embedding[:,1::2] = embedding_cos                       # At every second position starting from 1, put the cos values

        return pos_embedding        # pos_embeddings.shape = (seq_len + 1) x d_model
    
    

    def forward(self, images):
        # images.shape =  Batch_size x Channel x Hight x Width 
        batch_size = images.shape[0]
        
        patched_images = self.patch_images(images)                                                              # Transform the images from a batch into sequences
        proj_patch_images = self.linear_projection(patched_images)                                              # Linear Projection 
        embeddings = torch.cat((self.class_token.expand(batch_size,-1,-1), proj_patch_images), dim = 1)         # Add the class token embedding at first position in dimension 1
        pos_encoding  = self.create_position_encoding()                                                         # Create positional encoding
        embeddings = embeddings + pos_encoding                                                                  # Add the positional encoding to the embeddings
        

        return embeddings           # embeddings.shape = batch_size, (Number of total patches * Channels) + 1  x Length of embeddings after linear projection








class VisionTransformer(nn.Module):

    def __init__(
            self,
            dimensions,                   # (Channel,Hight,Width) of images
            num_patches,                  # Number of patches per dimension 
            embedding_size,               # Size of patch representation 
            num_attention_heads,          # Number of Self Attention Heads
            num_attention_layers,         # Number of Attention Layers
            lin_hidden_size,              # Hidden size of the FFN in the attention layers
    ):
        
        super(VisionTransformer,self).__init__()
        self.dimensions = dimensions
        self.num_patches = num_patches
        self.embedding_size = embedding_size
        self.num_attention_heads = num_attention_heads
        self.num_attention_layers = num_attention_layers
        self.lin_hidden_size = lin_hidden_size

        self.image_embedding = ImageEmbedding(
            dimensions          = self.dimensions,                        # (Channel,Width,Hight)
            num_patches         = self.num_patches,                       # Number of patches per dimension 
            proj_embedding_size = self.embedding_size                     # Length of patch embedding after linear projection
        )

        self.transformer_encoder = TransformerEncoder(
            input_size      = (self.num_patches**2)*self.dimensions[0] +1,   # Total number of patches * channels + class token
            embedding_size  = self.embedding_size,                           # Size of vector representation
            num_heads       = self.num_attention_heads,                      # Number of attention heads
            num_layers      = self.num_attention_layers,                     # Number of attention layers
            lin_hidden_size = self.lin_hidden_size                           # Hidden size of the FFN in the attention layers
        )




    
    def forward(self, images):
        # images.shape = batch_size x Channels x Hight x Width

        image_embeddings         = self.image_embedding(images)
        image_representations    = self.transformer_encoder(image_embeddings)
        representation_tokens    = image_representations[:,0,:]


        return representation_tokens     # shape: batch_size x embeddings_size 




        
        



        

        